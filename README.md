# Equilibrium Proxy

Equilibrium is a BigBlueButton Server proxy. Requires https://gitlab.com/tkrause/equilibrium-agent/

## Why a proxy?

- BigBlueButton by default is not very scalable to use it high CPU requirements for a small number of users per room. Only 200 users per server for a 4 core CPU.
- Large groups of meetings are not supported out of the box in BigBlueButton. Recommended one meeting at a time per 4 core CPU.
- Recordings are specific to the node they are on
- Meeting state is not shared across servers
- Many others...

## How does it work?

### What it does 👍

- Allows scaling BigBlueButton horizontally. 
   - Any number of servers can be added to support infinite meetings.
- Allows any number of recordings and ability to keep them indefinitely.
- Acts as a middle man between the client and multiple servers.
   - Servers can have the same shared secret or different ones. The proxy can expose them all through a single hostname and secret.
- Prevent down time during maintenance. The proxy will force all meetings to a new server if one goes down.

### What it cannot do 👎

- Allow infinite users in a single meeting. This is simply a limit from the server its running on. 
   - Scale up the individual nodes to support more users. 
   - BlindSide Networks claim 200 users per meeting on a 4 core CPU.

## Architecture

Equilibrium's architecture is pretty simple. 

### State

Redis is used to preserve the state of the proxy cluster across all nodes. No one node is responsible for state in this way allowing horizontal scaling.

### Proxy Cluster

Equilibrium consists of a proxy server written in Golang that is the brains of the operation.

The simple version is that is designates a BBB server for a meeting. Anyone joining that specific meeting will be forwarded to the selected BBB server.

Meeting IDs are unique across the cluster. No two separate servers can have the same meeting ID. 

This service can be horizontally scaled and was developed to run on Kubernetes or Docker.

The service receives requests, determines which end server it should go to based on meeting ID, and forwards back the response.

The server SHOULD (currently not implement) keep track of recordings in a GCS bucket and allow clients to be able to access recordings across all servers.

### Agent

An Equilibrium agent is required to run on each BBB worker node. This is to report the status of the node to the proxy server.

This agent should also be responsible for transcoding recordings and uploading to GCS bucket when the meeting is over.

The agent also reports all meetings in progress and when completed to the proxy server.

