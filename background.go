package main

import (
	"go-bbb-proxy/model"
	"go-bbb-proxy/model/redis"
	"log"
	"time"
)

const (
	PruneFrequency   = time.Minute
	ContactThreshold = time.Minute
	OfflineThreshold = time.Minute
	DeleteThreshold  = 30 * time.Minute
)

func BackgroundWorker() {
	log.Println("Background worker started...")
	for {
		// TODO: move sleep before work so that on startup nodes have time to check in before they get removed
		start := time.Now()
		work()
		duration := time.Since(start)

		log.Printf("Background worker completed in %v", duration)
		time.Sleep(PruneFrequency)
	}
}

func work() {
	lock, err := redis.Lock("background_lock", PruneFrequency)
	if err != nil {
		if err != redis.ErrNotObtained {
			log.Printf("Unable to obtain background worker lock: %v", err)
		}

		return
	}
	defer lock.Unlock()

	// get all nodes, if any haven't checked in
	// since the threshold then remove it
	nodes := model.AllServers()
	log.Printf("Obtained background lock: %v nodes: %v", lock.Token, len(nodes))

	for _, node := range nodes {
		// have we heard from this node recently?
		// if so, then it should remain up
		contacted := time.Unix(node.LastContactedAt,0)
		diff := time.Since(contacted)
		if diff <= ContactThreshold {
			continue
		}

		log.Printf("Node %v missed check-in. Last contact: %v ago", node.ID, diff)
		if diff >= DeleteThreshold {
			log.Printf("Removing node %v", node.ID)
			node.Delete()
		} else if node.Online && diff >= OfflineThreshold {
			log.Printf("Taking node %v offline", node.ID)
			node.TakeOffline()
		}
	}
}