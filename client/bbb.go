package client

import (
	"crypto/sha1"
	"encoding/hex"
	"github.com/go-resty/resty/v2"
	"go-bbb-proxy/model"
	"io"
	"net/url"
	"strings"
)

type BbbClient struct {
	client *resty.Client
}

var BBB = NewBBB()

func NewBBB() *BbbClient {
	client := resty.New()

	return &BbbClient{
		client: client,
	}
}

func (c *BbbClient) ProxyUrl(url url.URL, server *model.Server) (*resty.Response, error) {
	target := c.MakeUrlForProxy(url, server)
	return c.client.R().Get(target)
}

// TODO: make this work to support pre-upload of slides
// needs to forward the body payload with the same Content-Type header
func (c *BbbClient) ProxyUrlWithBody(url url.URL, server *model.Server, body *io.ReadCloser, header string) (*resty.Response, error) {
	target := c.MakeUrlForProxy(url, server)
	return c.client.R().
		SetHeader("Content-Type", header).
		SetBody(body).
		Post(target)
}

func (c *BbbClient) MakeUrlForProxy(url url.URL, server *model.Server) string {
	serverUrl, _ := url.Parse(server.Url)

	url.Scheme = serverUrl.Scheme
	url.Host = serverUrl.Host

	action := strings.Replace(url.Path,"/bigbluebutton/api/", "", 1)
	// delete any old checksum in the url
	q := url.Query()
	q.Del("checksum")

	bytes := sha1.Sum([]byte(action + q.Encode() + server.Secret))
	checksum := hex.EncodeToString(bytes[:])

	q.Add("checksum", checksum)
	url.Path = strings.TrimRight(serverUrl.Path, "/") + "/api/" + action
	url.RawQuery = q.Encode()

	return url.String()
}

//func (c *BbbClient) Uri(action string, server *model.Server, params *url.Values) string {
//	if params == nil {
//		params = &url.Values{}
//	}
//
//	action = strings.Trim(action, "/")
//
//	params.Del("checksum")
//	paramString := params.Encode()
//
//	hasher := sha1.New()
//	hasher.Write([]byte(action + paramString + server.Secret))
//	checksum := hex.EncodeToString(hasher.Sum(nil))
//
//	params.Add("checksum", checksum)
//
//	return strings.Trim(server.Url, "/") + "/" + action + "?" + params.Encode()
//}