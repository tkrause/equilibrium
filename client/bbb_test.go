package client

import (
	"github.com/stretchr/testify/assert"
	"go-bbb-proxy/model"
	"net/url"
	"strings"
	"testing"
)

var uri, _ = url.Parse("/bigbluebutton/api/findme")
var node = &model.Server{
	ID:     "test1",
	IP:     "127.0.0.1",
	Url:    "http://127.0.0.1:8080/b/api/",
	Secret: "12345678",
}

func TestBbbClient_makeUrlForProxy(t *testing.T) {
	actual := BBB.makeUrlForProxy(*uri, node)
	expected := "http://127.0.0.1:8080/b/api/findme"

	if ! strings.HasPrefix(actual, expected) {
		t.Fatalf("expected %v got %v", expected, actual)
	}

	actualUrl, _ := url.Parse(actual)
	checksum := actualUrl.Query().Get("checksum")
	expected = "28549e1a7346617e4c53ee1389d1b0c377603cac"

	assert.Equal(t, checksum, expected, "checksum error")
}
