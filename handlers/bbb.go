package handlers

import (
	"encoding/xml"
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"go-bbb-proxy/client"
	"go-bbb-proxy/model"
	"go-bbb-proxy/response"
	"log"
	"net/http"
)

const DefaultFormat = "xml"

func BbbIndexHandler(c *gin.Context) {
	c.XML(http.StatusOK, response.IndexResponse{
		Response: response.Response{
			ReturnCode: "SUCCESS",
		},
		Version: "2.2",
		Build:   "1",
	})
}

// very basic proxy, just grabs the meeting the user
// passes in and sends the request to the correct server
// handles errors if there's no meeting or server
func bbbMeetingProxy(c *gin.Context) (*model.Meeting, *resty.Response) {
	meeting := model.FindMeetingWithServer(c.Query("meetingID"))
	if meeting == nil {
		c.XML(http.StatusOK, response.MeetingNotFoundError)
		return nil, nil
	}

	resp, err := client.BBB.ProxyUrl(*c.Request.URL, meeting.Server)
	if err != nil {
		c.Error(err)
		c.XML(http.StatusOK, response.InternalError(err))
		return nil, resp
	}

	// be really stupid and just forward the exact response back
	c.Data(
		resp.StatusCode(),
		resp.Header().Get("Content-Type"),
		resp.Body(),
	)

	return meeting, resp
}

func BbbGetMeetingInfoHandler(c *gin.Context) {
	bbbMeetingProxy(c)
}

func BbbIsMeetingRunningHandler(c *gin.Context) {
	bbbMeetingProxy(c)
}

func BbbGetMeetingsHandler(c *gin.Context) {
	nodes := model.AllServers()

	var meetings []*response.BbbMeeting
	for _, node := range nodes {
		// ignore offline nodes
		if !node.Online {
			continue
		}

		// ignore servers with response errors
		resp, err := client.BBB.ProxyUrl(*c.Request.URL, node)
		if err != nil {
			c.Error(err)
			continue
		}

		// ignore xml parse errors
		var remoteResponse response.GetMeetingsResponse
		err = xml.Unmarshal(resp.Body(), &remoteResponse)
		if err != nil {
			c.Error(err)
			continue
		}

		// ignore servers with no meetings
		if len(remoteResponse.Meetings) <= 0 {
			continue
		}

		meetings = append(meetings, remoteResponse.Meetings...)
	}

	// build the response
	var resp response.GetMeetingsResponse
	if len(meetings) > 0 {
		resp = response.GetMeetingsResponse{
			Response: response.Response{ReturnCode: "SUCCESS"},
			Meetings: meetings,
		}
	} else {
		resp = response.NoMeetingsResponse
	}

	c.XML(http.StatusOK, resp)
}

func BbbCreateMeetingHandler(c *gin.Context) {
	node, err := model.FindAvailableServer()
	if err != nil {
		c.Error(err)
		c.XML(http.StatusOK, response.InternalError(err))
		return
	}

	// TODO: check to see if the meeting already exists and is on a server
	// also need to check that the server for that meeting still exists
	node.IncrementLoad()

	id := c.Query("meetingID")
	meeting, err := model.FindOrCreateMeeting(id, node)
	if err != nil {
		c.Error(err)
		c.XML(http.StatusOK, response.InternalError(err))
		return
	}

	// modify params and URL so we receive webhook callbacks
	// copy the current request URL
	u := *c.Request.URL
	target := "http://22d64dd9f4a7.ngrok.io" + "/callback/end"

	q := u.Query()
	q.Add("meta_endCallbackUrl", target)
	u.RawQuery = q.Encode()

	var resp *resty.Response
	if c.Request.Method == "POST" {
		resp, err = client.BBB.ProxyUrlWithBody(u, meeting.Server, &c.Request.Body, c.Request.Header.Get("Content-Type"))
	} else {
		resp, err = client.BBB.ProxyUrl(u, meeting.Server)
	}

	if err != nil {
		c.Error(err)
		c.XML(http.StatusOK, response.InternalError(err))
		return
	}

	// TODO: we may want to check that the remote server actually created the meeting
	// if not, we should probably clean it up here
	// if it did, then prevent new calls to create until the meeting ends

	// be really stupid and just forward the exact response back
	c.Data(
		resp.StatusCode(),
		resp.Header().Get("Content-Type"),
		resp.Body(),
	)
	/*def create
	params.require(:meetingID)

	begin
	server = Server.find_available
	rescue ApplicationRedisRecord::RecordNotFound
	raise InternalError, 'Could not find any available servers.'
	end

	# Create meeting in database
	logger.debug("Creating meeting #{params[:meetingID]} in database.")
	meeting = Meeting.find_or_create_with_server(params[:meetingID], server)

	# Update with old server if meeting already existed in database
	server = meeting.server

	logger.debug("Incrementing server #{server.id} load by 1")
	server.increment_load(1)

	duration = params[:duration].to_i

	# Set/Overite duration if MAX_MEETING_DURATION is set and it's greater than params[:duration] (if passed)
	if !Rails.configuration.x.max_meeting_duration.zero? &&
	(duration.zero? || duration > Rails.configuration.x.max_meeting_duration)
	logger.debug("Setting duration to #{Rails.configuration.x.max_meeting_duration}")
	params[:duration] = Rails.configuration.x.max_meeting_duration
	end

	logger.debug("Creating meeting #{params[:meetingID]} on BigBlueButton server #{server.id}")
	# Pass along all params except the built in rails ones
	uri = encode_bbb_uri('create', server.url, server.secret, pass_through_params)

	begin
	# Read the body if POST
	body = request.post? ? request.body.read : ''

	# Send a GET/POST request to the server
	response = get_post_req(uri, body)
	rescue BBBError
	# Reraise the error to return error xml to caller
	raise
	rescue StandardError => e
	logger.warn("Error #{e} creating meeting #{params[:meetingID]} on server #{server.id}.")
	raise InternalError, 'Unable to create meeting on server.'
	end

	# Render response from the server
	render(xml: response)
	end*/
}

func BbbEndMeetingHandler(c *gin.Context) {
	// normal proxy behavior but also delete the meeting
	meeting, r := bbbMeetingProxy(c)
	if r == nil || !r.IsSuccess() {
		return
	}

	var resp response.Response
	err := xml.Unmarshal(r.Body(), &resp)
	if err != nil {
		c.Error(err)
		return
	}

	// if the remote server succeeded we should too assuming
	// we have a meeting to delete
	if meeting == nil || resp.ReturnCode != "SUCCESS" {
		return
	}

	// if the meeting had a callback URL, let's attempt to hit it
	// otherwise if we wait for the callback URL, the meeting will be gone
	// there will be no callback url to look up
	meeting.Delete()
	if meeting.EndCallbackUrl != "" {
		resty.New().R().Get(meeting.EndCallbackUrl)
	}
}

func BbbJoinMeetingHandler(c *gin.Context) {
	id := c.Query("meetingID")

	meeting := model.FindMeetingWithServer(id)
	if meeting == nil {
		c.XML(http.StatusOK, response.MeetingNotFoundError)
		return
	}

	uri := client.BBB.MakeUrlForProxy(*c.Request.URL, meeting.Server)
	log.Println("join meeting at url: " + uri)
	c.Redirect(http.StatusTemporaryRedirect, uri)
}

// handle when a server kills a meeting
// instead of the API killing a meeting
func EndCallbackHandler(c *gin.Context) {
	id := c.Query("meetingID")
	meeting := model.FindMeeting(id)

	log.Println("hello from callback", meeting)

	if meeting == nil {
		c.Status(http.StatusOK)
		return
	}

	if meeting.EndCallbackUrl == "" {
		meeting.Delete()
		c.Status(http.StatusOK)
		return
	}

	// call the original callback that was given to us with the params we were sent
	resp, err := resty.New().R().Get(meeting.EndCallbackUrl + "?" + c.Request.URL.RawQuery)
	log.Println("hit callback: "+meeting.EndCallbackUrl, resp, err)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}

	meeting.Delete()
	c.Data(resp.StatusCode(), resp.Header().Get("Content-Type"), resp.Body())
}

func BbbProxy(c *gin.Context) {
	nodes := model.AllServers()
	resp, err := client.BBB.ProxyUrl(*c.Request.URL, nodes[0])
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	}

	c.Data(resp.StatusCode(), resp.Header().Get("Content-Type"), resp.Body())
}
