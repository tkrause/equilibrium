package handlers

import (
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

var router = SetupRouter()

func TestBbbIndexHandler(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/bigbluebutton/api/", nil)

	router.ServeHTTP(w, req)

	xml := "<response><returncode>SUCCESS</returncode><version>2.0</version><build>1</build></response>"
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, xml, w.Body.String())
}