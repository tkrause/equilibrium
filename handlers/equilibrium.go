package handlers

import (
	"github.com/gin-gonic/gin"
	"go-bbb-proxy/model"
	"go-bbb-proxy/response"
	"net/http"
	"time"
)

func NoRouteHandler(c *gin.Context) {
	c.XML(http.StatusOK, response.Error("unsupportedRequest", "This request is not supported."))
}

func HealthHandler(c *gin.Context) {
	servers := model.AllServers()
	resp := response.HealthResponse{
		Nodes:     servers,
		Available: model.AvailableServers(),
		Total:     len(servers),
	}
	c.JSON(http.StatusOK, resp)
}

func NodeHealthHandler(c *gin.Context) {
	id := c.Param("id")

	node := model.FindServer(id)
	if node == nil {
		c.Status(http.StatusNotFound)
		return
	}

	// hide the secret in this response since it's
	// public and requires no auth
	node.Secret = ""
	c.JSON(http.StatusOK, node)
}

func AgentHandler(c *gin.Context) {
	var server model.Server

	if err := c.BindJSON(&server); err != nil {
		return
	}

	//srv := model.NewServerFromRequest(&server)
	server.LastContactedAt = time.Now().Unix()
	server.Online = true
	if err := server.Save(); err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, server)
}

func AgentDeleteHandler(c *gin.Context) {
	c.Status(http.StatusOK)
}
