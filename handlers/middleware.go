package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"go-bbb-proxy/response"
	"go-bbb-proxy/security"
	"net/http"
)

func ChecksumMiddleware(config *viper.Viper) gin.HandlerFunc {
	return func(c *gin.Context) {
		secret := config.GetString("secret")
		if secret == "" {
			c.Next()
			return
		}

		checksum := security.Checksum(c.Request.URL, config.GetString("secret"))
		if checksum != c.Query("checksum") {
			c.XML(http.StatusOK, response.ChecksumError)
			c.Abort()
			return
		}

		c.Next()
	}
}

func MeetingIdMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Query("meetingID") == "" {
			c.XML(http.StatusOK, response.MissingMeetingIdError)
			c.Abort()
			return
		}

		c.Next()
	}
}