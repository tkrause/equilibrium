package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func SetupRouter(config *viper.Viper) *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	// Health Check for LB
	r.GET("/health", HealthHandler)
	r.GET("/health/:id", NodeHealthHandler)
	r.POST("/equilibrium/agent", AgentHandler)
	r.DELETE("/equilibrium/agent", AgentDeleteHandler)
	r.GET("/callback/end", EndCallbackHandler)

	// BigBlueButton APIs as of version 2.2
	// https://docs.bigbluebutton.org/dev/api.html#api-calls
	bbbApi := r.Group("/bigbluebutton/api/")
	bbbApi.Use(ChecksumMiddleware(config))
	{
		bbbApi.GET("/", BbbIndexHandler)
		bbbApi.GET("/getMeetings", BbbGetMeetingsHandler)

		bbbMeetApi := bbbApi.Group("/")
		bbbMeetApi.Use(MeetingIdMiddleware())
		{
			bbbMeetApi.GET("/isMeetingRunning", BbbIsMeetingRunningHandler)
			bbbMeetApi.GET("/getMeetingInfo", BbbGetMeetingInfoHandler)
			bbbMeetApi.GET("/create", BbbCreateMeetingHandler)
			bbbMeetApi.POST("/create", BbbCreateMeetingHandler)
			bbbMeetApi.GET("/end", BbbEndMeetingHandler)
			bbbMeetApi.GET("/join", BbbJoinMeetingHandler)
		}
	}

	r.NoRoute(NoRouteHandler)
	//get 'join', to: 'bigbluebutton_api#join'
	//get 'getRecordings', to: 'bigbluebutton_api#get_recordings', as: :get_recordings
	//get 'publishRecordings', to: 'bigbluebutton_api#publish_recordings', as: :publish_recordings
	//get 'updateRecordings', to: 'bigbluebutton_api#update_recordings', as: :update_recordings
	//get 'deleteRecordings', to: 'bigbluebutton_api#delete_recordings', as: :delete_recordings
	//end

	//get 'health_check', to: 'health_check#all'
	//
	//match '*any', via: :all, to: 'errors#unsupported_request'
	//root to: 'errors#unsupported_request', via: :all

	return r
}