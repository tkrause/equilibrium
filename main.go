package main

import (
	"github.com/spf13/viper"
	"go-bbb-proxy/handlers"
	"log"
)

var config = viper.New()

func main() {
	// uncomment for using a .env file
	// can be merged with a json config file as well

	//config.SetConfigType("dotenv")
	//config.SetConfigName(".env")
	//config.AddConfigPath(".")
	//config.AutomaticEnv()

	config.SetConfigName("config")
	config.AddConfigPath(".")

	// configuration file
	if err := config.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	listen := config.GetString("listen")
	if listen == "" {
		log.Fatal("listen string is not set in configuration file.")
	}

	// setup http routes, start background job, and start server
	r := handlers.SetupRouter(config)
	go BackgroundWorker()
	if err := r.Run(listen); err != nil {
		log.Fatal(err)
	}
}
