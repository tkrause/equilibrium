package model

import (
	"context"
	"fmt"
	"go-bbb-proxy/model/redis"
)

type Meeting struct {
	ID                string
	ServerID          string
	Server            *Server
	EndCallbackUrl    string
}

func FindOrCreateMeeting(meetingId string, node *Server) (*Meeting, error) {
	meeting := &Meeting{
		ID:                meetingId,
		ServerID:          node.ID,
		Server:            node,
		EndCallbackUrl:    "",
	}

	id, key := meeting.key()

	ctx := context.Background()
	rdb := redis.Redis().TxPipeline()
	rdb.HSetNX(ctx, key, "server_id", node.ID)

	if meeting.EndCallbackUrl != "" {
		rdb.HSetNX(ctx, key, "end_callback_url", meeting.EndCallbackUrl)
	}

	rdb.HGetAll(ctx, key)
	rdb.SAdd(ctx, "meetings", id)

	_, err := rdb.Exec(ctx)
	if err != nil {
		return nil, err
	}

	// TODO: handle the meeting already exists with a different server ID

	return meeting, nil
}

func AllMeetings() []string {
	ctx := context.Background()

	meetings, _ := redis.Redis().SMembers(ctx, "meetings").Result()

	return meetings
}

func FindMeeting(id string) *Meeting {
	ctx := context.Background()

	data, err := redis.Redis().HGetAll(ctx, "meeting:"+id).Result()
	if len(data) <= 0 || err != nil {
		return nil
	}

	return &Meeting{
		ID:                id,
		ServerID:          data["server_id"],
		EndCallbackUrl:    data["end_callback_url"],
	}
}

func FindMeetingWithServer(id string) *Meeting {
	meeting := FindMeeting(id)
	if meeting == nil {
		return nil
	}

	node := FindServer(meeting.ServerID)
	if node == nil {
		return nil
	}

	meeting.Server = node
	return meeting
}

func (m *Meeting) Save() error {
	ctx := context.Background()

	id, key := m.key()

	node := FindServer(m.ServerID)
	if node == nil {
		return fmt.Errorf("unable to find node with id '%v'", m.ServerID)
	}

	rdb := redis.Redis().TxPipeline()
	rdb.HSet(ctx, key, "server_id", m.ServerID)
	rdb.SAdd(ctx, "meetings", id)

	_, err := rdb.Exec(ctx)

	return err
}

func (m *Meeting) Delete() {
	ctx := context.Background()

	id, key := m.key()

	rdb := redis.Redis().TxPipeline()
	rdb.Del(ctx, key)
	rdb.SRem(ctx, "meetings", id)

	rdb.Exec(ctx)
}

func (m *Meeting) key() (string, string) {
	return m.ID, "meeting:" + m.ID
}
