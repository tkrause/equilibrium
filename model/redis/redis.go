package redis

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"github.com/go-redis/redis/v8"
	"io"
	"time"
)

var (
	rdb        *redis.Client
	luaRelease = redis.NewScript(`if redis.call("get", KEYS[1]) == ARGV[1] then return redis.call("del", KEYS[1]) else return 0 end`)
	ErrNotObtained = errors.New("redis: lock not obtained")
	ErrLockNotHeld = errors.New("redis: lock not held")
)

func Redis() *redis.Client {
	if rdb == nil {
		rdb = redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "", // no password set
			DB:       0,  // use default DB
		})
	}

	return rdb
}

type Locker struct {
	Key   string
	Token string
}

func Lock(key string, ttl time.Duration) (*Locker, error) {
	rdb := Redis()
	locker := &Locker{Key: key}

	if err := locker.generateToken(); err != nil {
		return nil, err
	}

	ok, err := rdb.SetNX(context.Background(), key, locker.Token, ttl).Result()
	if err != nil {
		return nil, err
	}

	if !ok {
		return nil, ErrNotObtained
	}

	return locker, nil
}

func (l *Locker) Unlock() error {
	res, err := luaRelease.Run(context.Background(), Redis(), []string{l.Key}, l.Token).Result()
	if err == redis.Nil {
		return ErrLockNotHeld
	} else if err != nil {
		return err
	}

	if i, ok := res.(int64); !ok || i != 1 {
		return ErrLockNotHeld
	}

	return nil
}

func (l *Locker) generateToken() error {
	data := make([]byte, 16)

	if _, err := io.ReadFull(rand.Reader, data); err != nil {
		return err
	}

	l.Token = base64.RawURLEncoding.EncodeToString(data)
	return nil
}
