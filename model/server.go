package model

import (
	"context"
	"errors"
	redisclient "github.com/go-redis/redis/v8"
	"go-bbb-proxy/model/redis"
	"log"
	"strconv"
)

type AgentRequest struct {
	ID     string
	Url    string
	Secret string
	IP     string
	Load   float64
}

type Server struct {
	ID               string  `json:"id"`
	IP               string  `json:"ip"`
	Url              string  `json:"url"`
	Secret           string  `json:"secret,omitempty"`
	Online           bool    `json:"online"`
	Load             float64 `json:"load"`
	LoadMultiplier   float64 `json:"load_multiplier"`
	LastContactedAt  int64   `json:"last_contacted_at"`
}

func AllServers() []*Server {
	rdb := redis.Redis()
	servers, _ := rdb.SMembers(context.Background(), "servers").Result()

	var nodes []*Server
	for _, id := range servers {
		node := FindServer(id)
		if node == nil {
			continue
		}

		nodes = append(nodes, node)
	}

	return nodes
}

func AvailableServers() int64 {
	rdb := redis.Redis()
	count, _ := rdb.SCard(context.Background(), "servers_enabled").Result()

	return count
}

func FindServer(id string) *Server {
	ctx := context.Background()
	rdb := redis.Redis().TxPipeline()

	getCmd := rdb.HGetAll(ctx, "server:"+id)
	load := rdb.ZScore(ctx, "server_load", id)
	_, err := rdb.Exec(ctx)

	if err != redisclient.Nil && err != nil {
		return nil
	}

	data := getCmd.Val()

	loadMultiplier, _ := strconv.ParseFloat(data["load_multiplier"], 64)
	online, _ := strconv.ParseBool(data["online"])
	lastContactedAt, _ := strconv.ParseInt(data["last_contacted_at"], 10, 64)

	srv := &Server{
		ID:               id,
		IP:               data["ip"],
		Url:              data["url"],
		Secret:           data["secret"],
		Online:           online,
		Load:             load.Val(),
		LoadMultiplier:   loadMultiplier,
		LastContactedAt:  lastContactedAt,
	}

	return srv
}

func FindAvailableServer() (*Server, error) {
	ctx := context.Background()
	res, err := redis.Redis().ZRangeWithScores(ctx, "server_load", 0, 100).Result()
	if err != nil {
		return nil, err
	} else if err == redisclient.Nil || len(res) <= 0 {
		return nil, errors.New("no servers available")
	}

	first := res[0]
	node := FindServer(first.Member.(string))
	if node == nil {
		return nil, errors.New("server went away while retrieving")
	}

	return node, nil
}

func (srv *Server) Save() error {
	ctx := context.Background()
	rdb := redis.Redis().TxPipeline()

	id, key := srv.key()

	rdb.HSet(ctx, key, map[string]interface{}{
		"ip":                srv.IP,
		"url":               srv.Url,
		"secret":            srv.Secret,
		"online":            srv.Online,
		"load_multiplier":   srv.LoadMultiplier,
		"last_contacted_at": srv.LastContactedAt,
	})
	rdb.SAdd(ctx, "servers", id)

	if srv.Online {
		rdb.SAdd(ctx, "servers_enabled", id)
		rdb.ZAdd(ctx, "server_load", &redisclient.Z{
			Score:  srv.Load,
			Member: id,
		})
	} else {
		rdb.SRem(ctx, "servers_enabled", id)
		rdb.ZRem(ctx, "server_load", id)
	}

	_, err := rdb.Exec(ctx)

	return err

	//server_key = key
	//redis.multi do
	//redis.hset(server_key, 'url', url) if url_changed?
	//redis.hset(server_key, 'secret', secret) if secret_changed?
	//redis.hset(server_key, 'online', online ? 'true' : 'false') if online_changed?
	//redis.hset(server_key, 'load_multiplier', load_multiplier) if load_multiplier_changed?
	//redis.sadd('servers', id) if id_changed?
	//if enabled_changed?
	//if enabled
	//	redis.sadd('server_enabled', id)
	//else
	//redis.srem('server_enabled', id)
	//redis.zrem('server_load', id)
	//end
	//end

	//if load_changed?
	//if load.present?
	//redis.zadd('server_load', load, id)
	//else
	//redis.zrem('server_load', id)
	//end
	//end
	//end
}

func (srv *Server) TakeOffline() {
	ctx := context.Background()
	rdb := redis.Redis()

	id, key := srv.key()
	log.Println(key)
	rdb.HSet(ctx, key, "online", false)
	rdb.ZRem(ctx, "server_load", id)

	srv.Online = false
}

func (srv *Server) Delete() {
	ctx := context.Background()
	rdb := redis.Redis().TxPipeline()

	id, key := srv.key()

	// clean up the dead server
	rdb.Del(ctx, key)
	rdb.SRem(ctx, "servers", id)
	rdb.SRem(ctx, "servers_enabled", id)
	rdb.ZRem(ctx, "server_load", id)

	rdb.Exec(ctx)
}

func (srv *Server) IncrementLoad() {
	rdb := redis.Redis()
	id, _ := srv.key()
	srv.Load, _ = rdb.ZIncr(context.Background(), "server_load", &redisclient.Z{
		Member: id,
	}).Result()
}

func (srv *Server) key() (string, string) {
	return srv.ID, "server:" + srv.ID
}
