package response

import (
	"encoding/xml"
	"go-bbb-proxy/model"
)

type Response struct {
	XMLName    xml.Name `xml:"response",json:"-"`
	ReturnCode string   `xml:"returncode,omitempty",json:"returncode"`
	MessageKey string   `xml:"messageKey,omitempty",json:"messageKey"`
	Message    string   `xml:"message,omitempty",json:"message"`
}

type IndexResponse struct {
	Response
	Version string `xml:"version",json:"version"`
	Build   string `xml:"build",json:"build"`
}

type HealthResponse struct {
	Nodes     []*model.Server
	Available int64
	Total     int
}

type IsMeetingRunningResponse struct {
	Response
	Running bool `xml:"running",json:"running"`
}

var (
	ChecksumError = Error(
		"checksumError",
		"You did not pass the checksum security check",
	)

	MissingMeetingIdError = Error(
		"missingParamMeetingID",
		"You must specify a meeting ID for the meeting.",
	)

	MeetingNotFoundError = Error(
		"notFound",
		"We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
	)
	NoMeetingsResponse = GetMeetingsResponse{
		Response: Response{
			ReturnCode: "SUCCESS",
			MessageKey: "noMeetings",
			Message:    "no meetings were found on this server",
		},
	}
)

type BbbMeeting struct {
	Xml string `xml:",innerxml"`
}

type GetMeetingsResponse struct {
	Response
	Meetings []*BbbMeeting `xml:"meetings>meeting"`
}

func Error(key string, message string) *Response {
	return &Response{
		ReturnCode: "FAILED",
		MessageKey: key,
		Message:    message,
	}
}

func InternalError(err error) *Response {
	return Error("internalError", err.Error())
}
