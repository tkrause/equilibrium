package security

import (
	"crypto/sha1"
	"encoding/hex"
	"net/url"
	"strings"
)

func Checksum(url *url.URL, secret string) string {
	action := strings.Replace(url.Path,"/bigbluebutton/api/", "", 1)

	// delete any old checksum in the url
	q := url.Query()
	q.Del("checksum")

	bytes := sha1.Sum([]byte(action + q.Encode() + secret))
	checksum := hex.EncodeToString(bytes[:])

	return checksum
}